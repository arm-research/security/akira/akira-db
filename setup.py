from setuptools import setup
from setuptools import find_packages
import os

lib_folder = os.path.dirname(os.path.realpath(__file__))
requirement_path = lib_folder + '/requirements.txt'
install_requires = []
if os.path.isfile(requirement_path):
    with open(requirement_path) as f:
        install_requires = f.read().splitlines()
else:
    raise FileNotFoundError

setup(
    name='akiraDB',
    author="Michael Bartling",
    author_email="michael.bartling@arm.com",
    version='0.0.1',
    packages=find_packages(),
    setup_requires=['six', 'wheel'],
    install_requires=install_requires
)
