# from abc import ABC, abstractmethod
from types import SimpleNamespace
from typing import List
import logging

logger = logging.getLogger(__name__)

LONG_WINDOW_THRESHOLD = 5000  # cycles
LONG_WINDOW = 50

LABEL_MAP = {0: "Benign", 1: "Flush", 2: "Spectre", 3: "Reload"}


def split_list(a_list):
    half = len(a_list) // 2
    return a_list[:half], a_list[half:]


class LabeledWindowNamespace(SimpleNamespace):
    pass


class LabeledWindowBase(LabeledWindowNamespace):
    def __init__(self, start, stop, label: int, soft_labels={}, label_map={}):
        self._r = range(start, stop + 1)
        self._label = int(label)
        self._soft_labels = soft_labels
        # Store label maps per labeled window after all. Useful if same "label" maps to different types
        # E.x. label=pid=1 -> "ls -l"
        self._label_map = label_map
        super().__init__()

    def get_label_map(self):
        return self._label_map

    @property
    def range(self):
        return self._r

    @property
    def label(self):
        return self._label

    @property
    def soft_labels(self):
        return self._soft_labels

    @property
    def label_str(self):
        LABEL_MAP = self.get_label_map()
        if LABEL_MAP:
            if self._label in LABEL_MAP:
                return LABEL_MAP[self._label]
            else:
                return "Label Not Found"
        else:
            return str(self._label)

    @property
    def is_leaf(self):
        return True

    def search(self, key):
        if key not in self.range:
            return 0
        else:
            return self.label

    def search_all(self, key):
        if key not in self.range:
            return 0, {}
        else:
            return self.label, self.soft_labels

    def __str__(self):
        return "{(%r), %s}" % (self.range, self.label_str)

    # def __repr__(self):
    #     return "%s(%s)" % (type(self).__name__, self.__dict__)


class LabeledWindow(LabeledWindowBase):
    """
    The serializiable base type that is our absolute unit of operation in Akira.
    """

    __version__ = "0.0.1a"

    def to_dict(self):
        return {"type": type(self).__name__, "version": self.__version__, "kwargs": self.__dict__}

#    def __repr__(self):
#        return repr(self)


class LabeledWindowFactory:
    """
    Deserialized LabeledWindows from DB
    """
    __version__ = "0.0.1a"

    @classmethod
    def make_one(cls, type_str, version, **kwargs):
        lw = LabeledWindow(0, 0, 0)
        lw.__dict__.update(**kwargs)
        return lw

# class SimpleSpectreLabeledWindow(LabeledWindowBase):
#
#     def get_label_map(self, label_map):
#         self.LABEL_MAP = {0: "Benign", 1: "Flush", 2: "Spectre", 3: "Reload"}
#         pass
#

# Typing


LabeledWindowList = List[LabeledWindow]


class BvhNode:
    """
    Internal BVH Nodes that actually construct the tree recursively
    TODO Probably will break if the windows are not disjoint
    """
    def __init__(self, labeledWindowList: LabeledWindowList):
        self._range = self.window_list_range(labeledWindowList)

        if len(labeledWindowList) == 1:
            self._left = labeledWindowList[0]
            self._right = None
        elif len(labeledWindowList) == 2:
            self._left = labeledWindowList[0]
            self._right = labeledWindowList[1]
        else:
            l, r = split_list(labeledWindowList)
            self._left = BvhNode(l)
            self._right = BvhNode(r)

    def window_list_range(self, labeledWindowList):
        # Note, window ranges are offset by 1 on the end (inclusive)
        return range(min(labeledWindowList[0].range),
                     max(labeledWindowList[-1].range) + 1)

    def search(self, key):
        if key not in self.range:
            return 0
        if self._left and key in self._left.range:
            return self._left.search(key)
        elif self._right and key in self._right.range:
            return self._right.search(key)
        # print("Weird search state")
        return 0

    def search_all(self, key):
        """
        Get List of labels, softlabels associated with spatial key
        """
        if key not in self.range:
            return 0, {}
        if self._left and key in self._left.range:
            return self._left.search_all(key)
        elif self._right and key in self._right.range:
            return self._right.search_all(key)
        # print("Weird search state")
        return 0, {}

    @property
    def is_leaf(self):
        return False

    @property
    def range(self):
        return self._range

    def __str__(self):
        return "{(%r), %s}" % (self.range, self.is_leaf)

    def __repr__(self):
        return "{(%r), %s}" % (self.range, self.is_leaf)


class WindowTree:
    """
    Store references to the labeled windows in a sorted (really partially
     ordered) list.
    Use to build a tree of the bounding windows by partitioning this list in
     half and storing the ranges
    """
    def __init__(self, labeledWindowList: LabeledWindowList = []):
        self._labeledWindows = labeledWindowList
        # Build the Tree
        self._root = BvhNode(labeledWindowList)

    def __getitem__(self, key):
        """
        Search the tree for a key in one of the window ranges,
        if a match return the label, else 0
        """
        return self._root.search(key)

    def get_all_labels_for_key(self, key):
        return self._root.search_all(key)
