from abc import ABC, abstractmethod
import logging
from .LabeledWindows import WindowTree, LabeledWindowList
from pandas import DataFrame

logger = logging.getLogger(__name__)


class TraceBase(ABC):

    def __init__(self, iden):
        self.id = iden
        self._d = None
        self._lw = []
        self._lw_df = None
        self._labels = set()
        self._pids = set()
        self._wt = None
        self._pid_lw = []
        self._pid_lw_df = None
        self._pcdf = None

    @abstractmethod
    def _get_data_frame(self) -> DataFrame:
        pass

    @abstractmethod
    def _get_labeled_windows(self) -> (DataFrame, LabeledWindowList):
        pass

    @abstractmethod
    def _get_pid_windows(self) -> (DataFrame, LabeledWindowList):
        pass

    @abstractmethod
    def _get_pc_data_frame(self) -> DataFrame:
        pass

    # Overridable
    def _get_labels(self):
        labels = set()
        for window in self.labeled_windows:
            labels.add(window.label)
        return labels

    # Overridable
    def _get_pids(self):
        pids = set()
        for window in self.pid_windows:
            pids.add(window.label)
        return pids

    def _get_windowTree(self):
        wt = WindowTree(self.labeled_windows)
        return wt

    @property
    def data_frame(self):
        # Cache the dataframe, only load when asked
        if self._d is None:
            logger.debug("DataFrame not cached for %s, loading", self.id)
            self._d = self._get_data_frame()
        return self._d

    @property
    def labeled_windows(self):
        if not self._lw:
            logger.debug("Windows not cached for %s, loading", self.id)
            self._lw_df, self._lw = self._get_labeled_windows()
        return self._lw

    @property
    def pid_windows(self):
        if not self._pid_lw:
            logger.debug("Windows not cached for %s, loading", self.id)
            self._pid_lw_df, self._pid_lw = self._get_pid_windows()
        return self._pid_lw

    @property
    def labels(self):
        if not self._labels:
            logger.debug("Labels not cached for %s, loading", self.id)
            self._labels = self._get_labels()
        return self._labels

    @property
    def pids(self):
        if not self._pids:
            logger.debug("PIDs not cached for %s, loading", self.id)
            self._pids = self._get_pids()
        return self._pids

    @property
    def windowTree(self):
        if self._wt is None:
            self._wt = self._get_windowTree()
        return self._wt

    @property
    def range(self):
        df = self.data_frame
        return range(df.index[0], df.index[-1])

    @property
    def PC_map_df(self):
        # Cache the dataframe, only load when asked
        if self._pcdf is None:
            logger.debug("PC DataFrame not cached for %s, loading", self.id)
            self._pcdf = self._get_pc_data_frame()
        return self._pcdf
