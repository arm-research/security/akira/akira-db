import configparser
import os
import logging

logger = logging.getLogger(__name__)


def get_default_config_from_ini():
    cparser = configparser.ConfigParser()
    cparser.read("public_config.ini")  # load shared dev
    cparser.read("private_config.ini")  # load sensitive bits
    config = {
        **cparser["DEFAULT"],  # load sensitive stuffs
        **os.environ,  # override loaded values with environment variables
    }
    return config


def get_MinIO_default_config_from_ini():
    config = get_default_config_from_ini()
    cparser = configparser.ConfigParser()
    cparser.read("public_config.ini")  # load shared dev
    client_config = {
        **cparser["DEFAULT"],
        "access_key": config["access_key"],
        "secret_key": config["secret_key"],
        # Handle Booleans
        "secure": cparser.getboolean("DEFAULT", "secure", fallback=False)
    }
    return client_config


def get_MinIO_default_config_from_env():
    def fetch_required_env_string(key):
        evar = os.environ.get(key, None)
        if evar is None:
            logger.error("Missing required environment variable {}".format(key))
            raise KeyError
        return evar

    def fetch_optional_env_str2bool(key):
        evar = os.environ.get(key, "")
        if evar.lower() == "true":
            return True
        else:
            logger.debug("Got unrecognized {} == {}, defaulting to False".format(key, evar))
            return False

    client_config = {
        "endpoint": fetch_required_env_string("AKIRA_MINIO_ENDPOINT"),
        "access_key": fetch_required_env_string("AKIRA_MINIO_ACCESS_KEY"),
        "secret_key": fetch_required_env_string("AKIRA_MINIO_SECRET_KEY"),
        # Handle Booleans
        "secure": fetch_optional_env_str2bool("MINIO_SECURE_MODE")
    }
    return client_config
