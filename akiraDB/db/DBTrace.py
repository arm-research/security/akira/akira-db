from akiraDB.core.types.Trace import TraceBase
from akiraDB.processing.LabeledWindows import load_windows_from_dataframe
import json
import logging
import traceback
from pandas import read_parquet, read_csv

logger = logging.getLogger(__name__)


class DBTrace(TraceBase):
    """
    Intermediary class for DB that provides basic static methods for all Object store interactions.
    """
    # Objects in S3 are simple directory style layout, so lets impose a standard on  object naming
    @staticmethod
    def ID_2_dataframe_object_id(iden):
        return iden + "__dataframe.parquet"

    @staticmethod
    def ID_2_labeled_windows_object_id(iden):
        return iden + "__l_windows.csv"

    @staticmethod
    def ID_2_PID_windows_object_id(iden):
        return iden + "__p_windows.csv"

    @staticmethod
    def ID_2_metadata_object_id(iden):
        return iden + "__meta_data.json"

    @staticmethod
    def ID_2_PC_map_id(iden):
        return iden + "__pc_mapper.parquet"


class ReadableDBTrace(DBTrace):
    """
    ReadOnly Database trace
    Note: Users should not have to create these classes directly, the DB will return them
    This is just a convenience class for accessing all the separate S3 objects. The users could always query objects directly from the client.
    """
    def __init__(self, symbol, bucket_name, akira_db):
        super().__init__(symbol)
        self._bucket_name = bucket_name
        self._db = akira_db

    def _get_s3_path(self, iden):
        return "s3://" + self._bucket_name + "/" + iden

    # Implement virtual getter functions. It is up to the user to handle exceptions
    def _get_data_frame(self):
        # Should we pull through the DB or via urllib3 directly + pandas?
        # # for now use the DB class as a proxy
        tgt_id = ReadableDBTrace.ID_2_dataframe_object_id(self.id)
        storage_options = self._db.populate_s3fs_storage_options()
        # try:
        #     response = self._db.get_object(self._bucket_name, tgt_id)
        #     df = read_parquet(response)
        # finally:
        #     response.close()
        #     response.release_conn()
        df = read_parquet(self._get_s3_path(tgt_id), storage_options=storage_options)
        return df

    def _get_labeled_windows(self):
        label_col = "label"
        lw = []
        tgt_id = ReadableDBTrace.ID_2_labeled_windows_object_id(self.id)
        storage_options = self._db.populate_s3fs_storage_options()
        # try:
        #     response = self._db.get_object(self._bucket_name, tgt_id)
        #     df = read_csv(response)
        # finally:
        #     response.close()
        #     response.release_conn()
        df = read_csv(self._get_s3_path(tgt_id), storage_options=storage_options)
        lw = load_windows_from_dataframe(df, label_col)
        return df, lw

    def _get_pid_windows(self):
        label_col = "pid"
        pw = []
        tgt_id = ReadableDBTrace.ID_2_PID_windows_object_id(self.id)
        storage_options = self._db.populate_s3fs_storage_options()
        # try:
        #     response = self._db.get_object(self._bucket_name, tgt_id)
        #     df = read_csv(response)
        # finally:
        #     response.close()
        #     response.release_conn()
        df = read_csv(self._get_s3_path(tgt_id), storage_options=storage_options)
        pw = load_windows_from_dataframe(df, label_col)
        return df, pw

    def read_metadata(self):
        tgt_id = ReadableDBTrace.ID_2_metadata_object_id(self.id)
        try:
            response = self._db.get_object(self._bucket_name, tgt_id)
            metadata = json.load(response)
        except Exception as e:
            logging.error(traceback.format_exc())
            raise e
        finally:
            response.close()
            response.release_conn()
        return metadata

    def _get_pc_data_frame(self):
        # Should we pull through the DB or via urllib3 directly + pandas?
        # for now use the DB class as a proxy
        tgt_id = ReadableDBTrace.ID_2_PC_map_id(self.id)
        storage_options = self._db.populate_s3fs_storage_options()
        # try:
        #     response = self._db.get_object(self._bucket_name, tgt_id)
        #     df = read_parquet(response)
        # finally:
        #     response.close()
        #     response.release_conn()
        df = read_parquet(self._get_s3_path(tgt_id), storage_options=storage_options)
        return df
