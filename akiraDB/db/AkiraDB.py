import logging
from akiraDB.core.types.Trace import TraceBase
from .DBTrace import ReadableDBTrace, DBTrace
from io import BytesIO
import json
from minio import Minio
import re
import os


logger = logging.getLogger(__name__)


def initdb(engine, **kwargs):
    pass


def cleardb(engine):
    logger.warn("Clear DB not implemented")


class AkiraDB(object):

    def __init__(self, minio_client: Minio):
        self._engine = minio_client
        self.initdb()

    def initdb(self):
        engine = self._engine
        initdb(engine)

    def cleardb(self):
        engine = self._engine
        cleardb(engine)

    @staticmethod
    def instantiate_empty_s3fs_storage_options():
        storage_options = {"endpoint_url": None, "key": None, "secret": None}
        return storage_options

    def populate_s3fs_storage_options(self):
        storage_options = AkiraDB.instantiate_empty_s3fs_storage_options()
        creds = self._engine._provider.retrieve()
        url = self._engine._base_url
        if url.is_https:
            endpoint_url = "https://" + url.host
        else:
            endpoint_url = "http://" + url.host
        storage_options["endpoint_url"] = endpoint_url
        storage_options["key"] = creds.access_key
        storage_options["secret"] = creds.secret_key
        return storage_options

    def list_traces(self, bucket_name="akira-default", prefix=None, recursive=False, start_after=None, include_user_meta=False, include_version=False, use_api_v1=False, use_url_encoding_type=True, dump_to_log=False):
        symbols = self.list_symbols(bucket_name, prefix, recursive, start_after, include_user_meta, include_version, use_api_v1, use_url_encoding_type)
        if dump_to_log:
            for symbol in symbols:
                trace = ReadableDBTrace(symbol, bucket_name, self)
                metadata = trace.read_metadata()
                # M5 logs can be huge, remove from the noise
                # metafilt = dict(filter(lambda x: x[0] not in ["m5log"], metadata.items()))
                metadata["m5log"] = "<redacted>"
                logger.info("%s %s", symbol, metadata)
        return symbols

    def list_symbols(self, bucket_name="akira-default", prefix=None, recursive=False, start_after=None, include_user_meta=False, include_version=False, use_api_v1=False, use_url_encoding_type=True, regex=None):
        """
        Return the symbols in this library. This is the prefix identifier for all Trace object groups

        :param bucket_name: Name of the bucket.
        :param prefix: Object name starts with prefix.
        :param recursive: List recursively than directory structure emulation.
        :param start_after: List objects after this key name.
        :param include_user_meta: MinIO specific flag to control to include
                                 user metadata.
        :param include_version: Flag to control whether include object
                                versions.
        :param use_api_v1: Flag to control to use ListObjectV1 S3 API or not.
        :param use_url_encoding_type: Flag to control whether URL encoding type
                                      to be used or not.
        :param regex: Additional string to filter returned objects
        :return: String list of symbols in the library

        Example::
            # List objects information.
            objects = client.list_objects("my-bucket")
            for obj in objects:
                print(obj)

            # List objects information whose names starts with "my/prefix/".
            objects = client.list_objects("my-bucket", prefix="my/prefix/")
            for obj in objects:
                print(obj)

            # List objects information recursively.
            objects = client.list_objects("my-bucket", recursive=True)
            for obj in objects:
                print(obj)

            # List objects information recursively whose names starts with
            # "my/prefix/".
            objects = client.list_objects(
                "my-bucket", prefix="my/prefix/", recursive=True,
            )
            for obj in objects:
                print(obj)

            # List objects information recursively after object name
            # "my/prefix/world/1".
            objects = client.list_objects(
                "my-bucket", recursive=True, start_after="my/prefix/world/1",
            )
            for obj in objects:
                print(obj)
        """

        objects = self.list_objects(bucket_name, prefix, recursive, start_after, include_user_meta, include_version, use_api_v1, use_url_encoding_type)
        symbols = set()
        for s3object in objects:
            if not s3object.is_dir:
                symbol = re.sub(r"__\w+\.\w+$", "", s3object.object_name)  # remove the tags
                if regex:
                    if re.search(regex, symbol):
                        symbols.add(symbol)
                else:
                    symbols.add(symbol)
        return symbols

    def has_symbol(self, symbol, bucket_name="akira-default", prefix=None, recursive=False, start_after=None, include_user_meta=False, include_version=False, use_api_v1=False, use_url_encoding_type=True):
        symbols = self.list_symbols(bucket_name, prefix, recursive, start_after, include_user_meta, include_version, use_api_v1, use_url_encoding_type)
        return symbol in symbols

    def read(self, symbol, bucket_name="akira-default") -> ReadableDBTrace:
        """
        Read proxy data for the named symbol. Returns a ReadableDBTrace object which can query objects

        """
        # TODO -> Pass ARGS and KWARGS to DBTrace
        return ReadableDBTrace(symbol, bucket_name, self)

    def read_metadata(self, symbol, bucket_name="akira-default"):
        """
        Return the metadata saved for a symbol.  This method is fast as it doesn't
        actually load the data.

        Parameters
        ----------
        :param symbol: symbol name for the item
        :param bucket_name: Bucket name to look in
        """
        trace = ReadableDBTrace(symbol, bucket_name, self)
        metadata = trace.read_metadata()
        return metadata

    def write(self, trace: TraceBase, metadata={}, bucket_name="akira-default", **kwargs):
        """
        Write Trace 'trace' under the specified 'symbol id' name to this library.

        Parameters
        ----------
        trace : TraceBase
            An Akira Trace object, note will locally force cache pull
        metadata : `dict`
            an optional dictionary of metadata to persist along with the symbol.
            Default: None
        kwargs :
            passed through to all of the put_object calls

        Returns
        -------
        success: `bool`
        """
        def fobject_len(f):
            pos = f.tell()
            f.seek(0, os.SEEK_END)
            nbytes = f.tell()
            f.seek(pos)
            return nbytes

        # Handle Caching
        _ = trace.data_frame
        _ = trace.labeled_windows
        _ = trace.pid_windows
        _ = trace.PC_map_df
        # Grab the internal dataframes
        _df = trace._d
        _ldf = trace._lw_df
        _pdf = trace._pid_lw_df
        _pcdf = trace._pcdf

        symbol = trace.id
        success = False

        tr_id = DBTrace.ID_2_dataframe_object_id(symbol)
        lw_id = DBTrace.ID_2_labeled_windows_object_id(symbol)
        pw_id = DBTrace.ID_2_PID_windows_object_id(symbol)
        md_id = DBTrace.ID_2_metadata_object_id(symbol)
        pc_id = DBTrace.ID_2_PC_map_id(symbol)

        # Trace Note BytesIO not playing nicely with fast parquet
        # Perk, use ephemeral storage instead of RAM for these ops
        # io = BytesIO()
        # _df.to_parquet(io, index=True)
        # io.seek(0)
        # response = self.engine.put_object(bucket_name, tr_id, io, -1)
        _df.to_parquet(tr_id, index=True)
        response = self._engine.fput_object(bucket_name, tr_id, tr_id)
        logger.debug(response)
        tr_success = True

        # Labeled Windows
        # io = StringIO()
        # _ldf.to_csv(io)
        # nbytes = fobject_len(io)
        # io.seek(0)
        # response = self._engine.put_object(bucket_name, lw_id, io, nbytes, content_type="text/csv")
        mbytes = _ldf.to_csv().encode("utf-8")
        buff = BytesIO(mbytes)
        response = self._engine.put_object(bucket_name, lw_id, buff, len(mbytes), content_type="text/csv")
        logger.debug(response)
        lw_success = True

        # PID Windows
        # io = StringIO()
        # _pdf.to_csv(io)
        # nbytes = fobject_len(io)
        # io.seek(0)
        # response = self._engine.put_object(bucket_name, pw_id, io, nbytes, content_type="text/csv")
        mbytes = _pdf.to_csv().encode("utf-8")
        buff = BytesIO(mbytes)
        response = self._engine.put_object(bucket_name, pw_id, buff, len(mbytes), content_type="text/csv")
        logger.debug(response)
        pw_success = True

        # Metadata
        io = BytesIO()
        dlen = io.write(json.dumps(metadata).encode())
        io.seek(0)
        response = self._engine.put_object(bucket_name, md_id, io, dlen, content_type="application/json")
        logger.debug(response)
        md_success = True

        # PC Trace
        # Note BytesIO not playing nicely with fast parquet
        # Perk, use ephemeral storage instead of RAM for these ops
        # io = BytesIO()
        # _pcdf.to_parquet(io, index=True)
        # io.seek(0)
        # response = self.engine.put_object(bucket_name, pc_id, io, -1)
        _pcdf.to_parquet(pc_id, index=True)
        response = self._engine.fput_object(bucket_name, pc_id, pc_id)
        logger.debug(response)
        pc_success = True

        # Handle return
        success = tr_success and lw_success and pw_success and pc_success and md_success
        status = {
            "Trace Upload": "Success" if tr_success else "Failed",
            "Labeled Windows Upload": "Success" if lw_success else "Failed",
            "PID Windows Upload": "Success" if pw_success else "Failed",
            "PC Trace Upload": "Success" if pc_success else "Failed",
            "Metadata Upload": "Success" if md_success else "Failed"
        }
        if not success:
            # TODO clear elements?
            logger.error(status)
        else:
            logger.debug(status)
        return success

    def delete(self, symbol, bucket_name="akira-default"):
        """
        Delete all versions of the item from the current library which aren't
        currently part of some snapshot.

        Parameters
        ----------
        symbol : `str`
            symbol name to delete
        """
        tr_id = DBTrace.ID_2_dataframe_object_id(symbol)
        lw_id = DBTrace.ID_2_labeled_windows_object_id(symbol)
        pw_id = DBTrace.ID_2_PID_windows_object_id(symbol)
        md_id = DBTrace.ID_2_metadata_object_id(symbol)
        pc_id = DBTrace.ID_2_PC_map_id(symbol)
        delete_object_list = [tr_id, lw_id, pw_id, md_id, pc_id]
        self.engine.remove_objects(bucket_name, delete_object_list)

    def list_objects(self, bucket_name="akira-default", prefix=None, recursive=False, start_after=None, include_user_meta=False, include_version=False, use_api_v1=False, use_url_encoding_type=True, regex=None):
        """
        Return the objects in a bucket. Wrapper for minio.list_objects

        :param bucket_name: Name of the bucket.
        :param prefix: Object name starts with prefix.
        :param recursive: List recursively than directory structure emulation.
        :param start_after: List objects after this key name.
        :param include_user_meta: MinIO specific flag to control to include
                                 user metadata.
        :param include_version: Flag to control whether include object
                                versions.
        :param use_api_v1: Flag to control to use ListObjectV1 S3 API or not.
        :param use_url_encoding_type: Flag to control whether URL encoding type
                                      to be used or not.
        :param regex: Additional string to filter returned objects
        :return: String list of symbols in the library

        Example::
            # List objects information.
            objects = client.list_objects("my-bucket")
            for obj in objects:
                print(obj)

            # List objects information whose names starts with "my/prefix/".
            objects = client.list_objects("my-bucket", prefix="my/prefix/")
            for obj in objects:
                print(obj)

            # List objects information recursively.
            objects = client.list_objects("my-bucket", recursive=True)
            for obj in objects:
                print(obj)

            # List objects information recursively whose names starts with
            # "my/prefix/".
            objects = client.list_objects(
                "my-bucket", prefix="my/prefix/", recursive=True,
            )
            for obj in objects:
                print(obj)

            # List objects information recursively after object name
            # "my/prefix/world/1".
            objects = client.list_objects(
                "my-bucket", recursive=True, start_after="my/prefix/world/1",
            )
            for obj in objects:
                print(obj)
        """
        return self._engine.list_objects(bucket_name, prefix, recursive, start_after, include_user_meta, include_version, use_api_v1, use_url_encoding_type)

    def get_object(self, bucket_name, object_name, offset=0, length=0, request_headers=None, ssec=None, version_id=None, extra_query_params=None):
        """
        Get data of an object. Returned response should be closed after use to
        release network resources. To reuse the connection, it's required to
        call `response.release_conn()` explicitly.

        :param bucket_name: Name of the bucket.
        :param object_name: Object name in the bucket.
        :param offset: Start byte position of object data.
        :param length: Number of bytes of object data from offset.
        :param request_headers: Any additional headers to be added with GET
                                request.
        :param ssec: Server-side encryption customer key.
        :param version_id: Version-ID of the object.
        :param extra_query_params: Extra query parameters for advanced usage.
        :return: :class:`urllib3.response.HTTPResponse` object.

        Example::
            # Get data of an object.
            try:
                response = client.get_object("my-bucket", "my-object")
                # Read data from response.
            finally:
                response.close()
                response.release_conn()

            # Get data of an object of version-ID.
            try:
                response = client.get_object(
                    "my-bucket", "my-object",
                    version_id="dfbd25b3-abec-4184-a4e8-5a35a5c1174d",
                )
                # Read data from response.
            finally:
                response.close()
                response.release_conn()

            # Get data of an object from offset and length.
            try:
                response = client.get_object(
                    "my-bucket", "my-object", offset=512, length=1024,
                )
                # Read data from response.
            finally:
                response.close()
                response.release_conn()

            # Get data of an SSE-C encrypted object.
            try:
                response = client.get_object(
                    "my-bucket", "my-object",
                    ssec=SseCustomerKey(b"32byteslongsecretkeymustprovided"),
                )
                # Read data from response.
            finally:
                response.close()
                response.release_conn()
        """
        return self._engine.get_object(bucket_name, object_name, offset, length, request_headers, ssec, version_id, extra_query_params)
