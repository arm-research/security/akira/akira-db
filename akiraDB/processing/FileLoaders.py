from akiraDB.core.types.LabeledWindows import LabeledWindow, WindowTree
from akiraDB.core.types.Trace import TraceBase
from .LabeledWindows import load_windows, labeledWindowList2DataFrame, load_pid_windows, load_labeled_windows
from collections import defaultdict
import glob
import gzip
import os
import logging
import pandas as pd

logger = logging.getLogger(__name__)
BENIGN = 0


def hex2int(x):
    return int(x, 16)


def load_trace(root, bname, boot_crap_offset=10000, drop_op=True):
    import pandas as pd
    bname = os.path.basename(bname)
    if ".log" in bname:
        bname = bname.rsplit(".", 1)[0]
    path = os.path.join(root, "%s.log" % bname)
    path_l = os.path.join(root, "%s-labels.log" % bname)
    logger.info("Loading Data %s", path)

    d = pd.read_csv(path, index_col="c")
    # Get the labeled windows too
    logger.info("Loading Windows %s", path_l)
    if os.path.exists(path_l):
        labeled_windows = load_windows(path_l)
    else:
        logger.warning("%s doesnt exist, defaulting to Benign", path_l)
        labeled_windows = []
    # Default to benign if empty file
    if not labeled_windows:
        labeled_windows = [LabeledWindow(d.index[0], d.index[-1], BENIGN)]

    wt = WindowTree(labeled_windows)
    return (d, labeled_windows, wt)


def get_cycle_Bounds(fname):
    fname_gz = fname + ".gz"
    if os.path.exists(fname_gz):
        logger.debug("Getting cycle bounds for {}".format(fname_gz))
        with gzip.open(fname_gz, mode="rt") as fp:
            next(fp)  # Skip the header line
            start = int(next(fp).split(",")[0])
            for line in fp:
                end = int(line.split(",")[0])
    else:
        logger.debug("Getting cycle bounds for {}".format(fname))
        with open(fname) as fp:
            next(fp)  # Skip the header line
            start = int(next(fp).split(",")[0])
            for line in fp:
                end = int(line.split(",")[0])
    return (start, end)


class TraceFile(TraceBase):
    """
    TODO: Add label map capability so we can map window integers to some string or other int (To make experimental setup easy)
    """
    def __init__(self, bname, default_benign_label=BENIGN, force_benign=False):
        if ".log" in bname:
            bname = os.path.splitext(bname)[0]
        iden = os.path.basename(bname)
        super().__init__(iden)
        self.bname = bname
        self.default_benign_label = default_benign_label
        self.force_benign = force_benign  # can force this Trace to load its windows as benign
        self.trace_file = bname + ".log"
        self.window_file = bname + "-labels.log"
        self.pid_file = bname + "_pid_windows.csv"
        self.m5_file = bname + "-m5.log"
        self.trace_pc_file = bname + "_pc.log"

    def _get_data_frame(self):
        """
        2 Requirements for the CSV. First, the first column MUST be the index
        and 2, converters must include every hextype that needs to be converted to an integer form
        """
        tfile = self.trace_file
        compression = 'infer'
        converters = {
            "pc": hex2int
            # "cpu_cluster[0].cpus[0]:ActiveCycles": hex2int,
            # "c": hex2int
        }
        if os.path.exists(self.trace_file + ".gz"):
            compression = "gzip"
            tfile += ".gz"

        # This DataFrame could also include the PC values, lets not keep these around as they can be huge
        df = pd.read_csv(tfile, index_col=0, compression=compression, converters=converters)
        # Skip the first line as it containes the sum of all PMU events prior
        df = df.iloc[1:]
        cols = list(df.columns)
        if "pc" in cols:
            # Go ahead and cache the PC Series
            self._pcdf = pd.DataFrame(df["pc"])
            cols.remove("pc")
            return df[cols]
        return df

    def _get_labeled_windows(self):
        # TODO Add capability to pass in malicious labels/override for empty window file
        lw = []
        df = None
        if self.force_benign:
            logger.warning("Forcing %s to Benign", self.bname)
            # d = pd.read_csv(self.trace_file, index_col="c")  # temporarily load trace
            start, end = get_cycle_Bounds(self.trace_file)
            # Go ahead and set the target window as the entire trace
            # self._lw = [LabeledWindow(d.index[0], d.index[-1], self.default_benign_label)]
            lw = [LabeledWindow(start, end, self.default_benign_label)]
            df = labeledWindowList2DataFrame(lw)

        elif os.path.exists(self.window_file) and os.stat(self.window_file).st_size != 0:
            df, lw = load_labeled_windows(self.window_file)
        else:
            logger.warning("%s doesnt exist, defaulting to Benign", self.bname)
            # d = pd.read_csv(self.trace_file, index_col="c")  # temporarily load trace
            start, end = get_cycle_Bounds(self.trace_file)
            # Go ahead and set the target window as the entire trace
            # self._lw = [LabeledWindow(d.index[0], d.index[-1], self.default_benign_label)]
            lw = [LabeledWindow(start, end, self.default_benign_label)]
            df = labeledWindowList2DataFrame(lw)
        return df, lw

    def _get_pid_windows(self):
        if not os.path.exists(self.pid_file):
            logger.warn("Pid Window file not found -> defaulting to empty")
            return None, []
        return load_pid_windows(self.pid_file)

    def _get_pc_data_frame(self):
        tfile = self.trace_pc_file
        compression = 'infer'
        converters = {
            "pc": hex2int
            # "cpu_cluster[0].cpus[0]:ActiveCycles": hex2int,
            # "c": hex2int
        }
        if os.path.exists(self.trace_file + ".gz"):
            compression = "gzip"
            tfile += ".gz"
        if os.path.exists(tfile):
            # This DataFrame could also include the PC values, lets not keep these around as they can be huge
            df = pd.read_csv(tfile, index_col=0, compression=compression, converters=converters)
            # Skip the first line as it containes the sum of all PMU events prior
            df = df.iloc[1:]
        else:
            # see if we are in the dataframe
            _ = self.data_frame
            # If we cant find it in the data frame create a dummy
            if not self._pcdf:
                logger.warn("PC not found in Trace File, defaulting to empty Series")
                # self._pcdf = pd.Series()
                df = pd.DataFrame(columns=["pc"])
                df.index.name = "c"
        return df

    def read_m5_log(self):
        if not os.path.exists(self.m5_file):
            logger.warning("Missing M5 log")
            return ""
        else:
            with open(self.m5_file) as fp:
                m5Log = fp.read()
            return m5Log


class WindowScanner:
    """
    This class will scan a root directory for all its declared window types and build a
    simple internal database of their windows. This makes it easy to post sample.
    """
    def __init__(self, default_benign_label=BENIGN, benign_window_mu=200, benign_window_sigma=30):
        self.default_benign_label = BENIGN
        # Benign windows need to be randomly sampled from the traces
        self.benign_window_mu = benign_window_mu
        self.benign_window_sigma = benign_window_sigma

    def find_windows(self, root):
        path = os.path.join(root, "*.log")
        fnames = glob.glob(path)
        fnames = filter(lambda fname: "-labels" not in fname, fnames)
        # bnames = list(map(lambda x: x.rsplit(".", 1)[0], fnames))  # Remove the extension
        bnames = list(map(lambda x: os.path.splitext(x)[0], fnames))  # Remove the extension

        label2TraceMap = defaultdict(list)
        for bname in bnames:
            logger.debug("Scanner found %s", bname)

        for bname in bnames:
            # Stupid hack
            force_benign = False
            if "-na.out" in bname:
                force_benign = True

            trace = TraceFile(bname, force_benign=force_benign)
            for lbl in trace.labels:
                label2TraceMap[lbl].append(trace)
        return label2TraceMap
