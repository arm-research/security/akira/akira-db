import itertools
import logging
import numpy as np
from pandas import DataFrame, read_csv
from akiraDB.core.types.LabeledWindows import LabeledWindow, LabeledWindowList

logger = logging.getLogger(__name__)

LONG_WINDOW_THRESHOLD = 5000  # cycles
LONG_WINDOW = 50
LONG_WINDOW_KEY = "LongWindow"

LABEL_MAP = {0: "Benign", 1: "Flush", 2: "Spectre", 3: "Reload"}


# def load_windows(fn, long_window_threshold=LONG_WINDOW_THRESHOLD):
#     windowList = []
#     with open(fn) as fp:
#         while True:
#             line1 = fp.readline()
#             line2 = fp.readline()
#             if not line2:
#                 # print("ERROR, odd number of lines")
#                 break  # EOF
#             start, label, _ = line1.split(',')
#             stop, l2, _ = line2.split(',')
#             soft_labels = []
#             if label != l2:
#                 print("Error, label mismatch")
#             if int(stop) - int(start) > long_window_threshold:
#                 soft_labels.append(LONG_WINDOW)
#             w = LabeledWindow(int(start), int(stop), label, soft_labels)
#             windowList.append(w)
#     return windowList

class LabeledWindowException(Exception):
    pass


def load_windows_from_dataframe(df: DataFrame, label_col, window_col="window", long_window_threshold=LONG_WINDOW_THRESHOLD, label2Int=int):
    windowList = []
    # TODO, while loop this next if else until we have a valid last row
    last_row = df.iloc[-1]
    if last_row[window_col] != 'e':
        logger.warning("Corrupted last row in Window Dataframe (dropping): %s", last_row)
        # This explodes when the last index is duplicated for opmode stuff
        # df.drop(index=df.index[-1], inplace=True)
        # LOL HACK
        df = df.iloc[0:-1]
    else:
        # Make sure last row isnt corrupted
        filt = last_row.isnull()
        for (idx, isna) in zip(last_row.index, filt):
            if idx == window_col and isna:
                logger.error("Corrupted last row window id in window Dataframe")
                raise LabeledWindowException
            if isna:
                logger.warn("Corrupted last row element %s, defaulting to previous row", idx)
                df.iat[-1, df.columns.get_loc(idx)] = df.iat[-2, df.columns.get_loc(idx)]
    rng = np.arange(len(df)) // 2
    group_len = len(rng) // 2
    # Ignore the last row
    if len(rng) % 2:
        oddLen = True
    for i, g in df.groupby(rng):
        row1 = g.iloc[0]
        start = int(g.index[0])
        # If the file is corrupted, then ignore the last window
        if i == group_len and oddLen:
            # TODO Fixme
            continue
            # row2 = row1
            stop = df.index[-1]
            label = label2Int(row1[label_col])
            soft_labels = dict(row1.drop([label_col, window_col]))
            w = LabeledWindow(start, stop, label, soft_labels)
            windowList.append(w)
        else:
            row2 = g.iloc[1]
            stop = int(g.index[1])
        if row1[window_col] != 'b':
            logger.error("Invalid window occured, expected b: %s", g)
            raise LabeledWindowException
        if row2[window_col] != 'e':
            logger.error("Invalid window occured, expected e: %s", g)
            raise LabeledWindowException
        if row1[label_col] != row2[label_col]:
            logger.error("Invalid window occured, expected matching labels: %s", g)
            raise LabeledWindowException
        label = label2Int(row1[label_col])
        soft_labels = dict(row1.drop([label_col, window_col]))
        if stop - start > long_window_threshold:
            soft_labels[LONG_WINDOW_KEY] = LONG_WINDOW
        w = LabeledWindow(start, stop, label, soft_labels)
        windowList.append(w)
    return windowList


def grouper(iterable, n, *, incomplete='fill', fillvalue=None):
    "Collect data into non-overlapping fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, fillvalue='x') --> ABC DEF Gxx
    # grouper('ABCDEFG', 3, incomplete='strict') --> ABC DEF ValueError
    # grouper('ABCDEFG', 3, incomplete='ignore') --> ABC DEF
    args = [iter(iterable)] * n
    if incomplete == 'fill':
        return itertools.zip_longest(*args, fillvalue=fillvalue)
    if incomplete == 'strict':
        return zip(*args, strict=True)
    if incomplete == 'ignore':
        return zip(*args)
    else:
        raise ValueError('Expected fill, strict, or ignore')


def load_labeled_windows_dict_from_csv_no_overlap(fname):
    # Non-composable windows, all b are followed by an e
    labeled_windows = []
    with open(fname) as fp:
        columns = next(fp).strip().split(",")
        index = 0
        window_indicator_col = columns.index("b-e")
        label_cols = [i for i in range(1, len(columns)) if i != window_indicator_col]
        for b_row, e_row in grouper(fp, 2, incomplete='ignore'):
            b_row = b_row.strip().split(",")
            e_row = e_row.strip().split(",")
            assert b_row[window_indicator_col] == 'b'
            assert e_row[window_indicator_col] == 'e'
            for label_col in label_cols:
                assert b_row[label_col] == e_row[label_col]
            W = {
                columns[i]: e_row[i] for i in label_cols
            }
            W['start'] = int(b_row[index])
            W['stop'] = int(e_row[index])
            labeled_windows.append(W)
    return labeled_windows


def load_labeled_windows_dict_from_dataframe_no_overlap(df):
    # Non-composable windows, all b are followed by an e
    labeled_windows = []
    columns = df.columns
    index = 'cycle'
    window_indicator_col = 'window'
    label_cols = [c for c in columns if c not in [index, window_indicator_col]]
    for (_, b_row), (_, e_row) in grouper(df.iterrows(), 2, incomplete='ignore'):
        assert b_row[window_indicator_col] == 'b'
        assert e_row[window_indicator_col] == 'e'
        for label_col in label_cols:
            assert b_row[label_col] == e_row[label_col]
        W = {
            i: b_row[i] for i in label_cols
        }
        W['start'] = int(b_row[0])
        W['stop'] = int(e_row[0])
        labeled_windows.append(W)
    return labeled_windows


def load_labeled_windows(fn, long_window_threshold=LONG_WINDOW_THRESHOLD):
    label_col = "label"
    df = read_csv(fn, index_col="cycle", names=["cycle", label_col, "window"], header=None)
    return df, load_windows_from_dataframe(df, label_col)


def load_pid_windows(fn, long_window_threshold=LONG_WINDOW_THRESHOLD):
    label_col = "pid"
    df = read_csv(fn, index_col="cycle", names=["cycle", "window", "opmode", label_col], header=0)
    return df, load_windows_from_dataframe(df, label_col)


def load_windows(fn, long_window_threshold=LONG_WINDOW_THRESHOLD):
    _, lw = load_labeled_windows(fn, long_window_threshold)
    return lw


def labeledWindowList2DataFrame(lws: LabeledWindowList, label_tag="label") -> DataFrame:
    lwdicts = []
    for w in lws:
        start, stop = min(w.range), max(w.range)
        begin = {"cycle": start, label_tag: w.label, "window": 'b'}
        endof = {"cycle": stop, label_tag: w.label, "window": 'e'}
        sl = w.soft_labels
        sl.pop(LONG_WINDOW_KEY, None)
        begin.update(sl)
        endof.update(sl)
        lwdicts.append(begin)
        lwdicts.append(endof)
    return DataFrame(lwdicts).set_index("cycle")
