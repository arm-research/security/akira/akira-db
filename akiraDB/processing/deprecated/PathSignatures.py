from akira.core.types.LabeledWindows import WindowTree, LabeledWindowBase, LabeledWindow
from .LabeledWindows import load_windows
from .FileLoaders import load_trace

from collections import defaultdict
import iisignature as iis
import numpy as np
import pandas as pd
import random

def gauss_filter(width, sigma, alpha=1):
    x = np.linspace(-3 * sigma, 3 * sigma, width)
    return alpha * np.exp(-x**2 / (2 * sigma**2)) / (np.sqrt(2 * np.pi) * sigma)


def get_window_signatures(ds,
                          lws,
                          signature_levels=3,
                          window_len=400,
                          filternan=True,
                          gauss_width=None,
                          time_augmentation=True,
                          prescale=True):
    sigs = []
    idx = []
    # TODO Check boundry effects here. Because we are pre-windowing this might lead to extra distinction between mal/ben
    if gauss_width:
        ds = ds.fillna(0).transform(lambda x: np.convolve(
            x, gauss_filter(gauss_width, 1), mode="same"))
    if prescale:
        alpha = np.math.factorial(signature_levels)**(1 / signature_levels)
        ds = alpha * ds
    # import pdb; pdb.set_trace()
    for w in lws:
        for b in range(min(w.range), max(w.range), window_len):
            window_range = range(b, b + window_len)
            dw1 = ds.reindex(window_range)
            if filternan:
                if dw1.isnull().all(None):
                    continue
            dw1 = dw1.fillna(0)
            if time_augmentation:
                # Can either either augment base 0 linspace or cycle counts, doesnt really matter
                # dw2 = np.hstack((np.arange(dw1.shape[0]).reshape(-1,1), dw1))
                dw2 = np.hstack((np.arange(dw1.index[0],
                                           dw1.index[-1] + 1).reshape(-1,
                                                                      1), dw1))
            else:
                dw2 = dw1
            sig = iis.sig(dw2, signature_levels)
            # Hack for now
            if np.sum(np.abs(sig)) < 0.0000001:
                # import pdb; pdb.set_trace()
                continue
            sigs.append(sig)
            idx.append((dw1.index[0], dw1.index[-1]))
    # print((sigs, idx))
    return (sigs, idx)


def yield_window_signatures(ds,
                            signature_levels=3,
                            window_len=10000,
                            filternan=True,
                            gauss_width=None,
                            time_augmentation=True,
                            prescale=True,
                            log_sig=True):
    # TODO Check boundry effects here. Because we are pre-windowing this might lead to extra distinction between mal/ben
    # Note: Rolling convolution is feasible in practice
    if gauss_width:
        ds = ds.fillna(0).transform(lambda x: np.convolve(
            x, gauss_filter(gauss_width, 1), mode="same"))
    if prescale:
        alpha = np.math.factorial(signature_levels)**(1 / signature_levels)
        ds = alpha * ds
    if log_sig:
        ncols = ds.shape[1]
        if time_augmentation:
            ncols += 1
        prep = iis.prepare(ncols, signature_levels)
    for b in range(ds.index[0], ds.index[-1], window_len):
        window_range = range(b, b + window_len)
        dw1 = ds.reindex(window_range)
        if filternan:
            if dw1.isnull().all(None):
                continue
        dw1 = dw1.fillna(0)
        if time_augmentation:
            # Can either either augment base 0 linspace or cycle counts, doesnt really matter
            # dw2 = np.hstack((np.arange(dw1.shape[0]).reshape(-1,1), dw1))
            dw2 = np.hstack((np.arange(dw1.index[0],
                                       dw1.index[-1] + 1).reshape(-1, 1), dw1))
        else:
            dw2 = dw1
        if log_sig:
            sig = iis.logsig(dw2, prep)
        else:
            sig = iis.sig(dw2, signature_levels)
        # Hack for now
        if np.sum(np.abs(sig)) < 0.0000001:
            # import pdb; pdb.set_trace()
            continue
        idxs = (dw1.index[0], dw1.index[-1])
        yield (sig, idxs, dw2)
    # return (sigs, idx)


def get_local_signatures(ds, 
                         base, 
                         signature_levels,
                         window_len,
                         filternan,
                         time_augmentation,
                         log_sig,
                         log_sig_prep):
    window_range = range(base, base + window_len)
    if max(window_range) >= len(ds) or min(window_range) < 0:
        return (None, None, None)

    dw1 = ds.iloc[window_range]
    if filternan:
        if dw1.isnull().all(None):
            return (None, None, None)
    dw1 = dw1.fillna(0)
    if time_augmentation:
        # Can either either augment base 0 linspace or cycle counts, doesnt really matter
        # dw2 = np.hstack((np.arange(dw1.shape[0]).reshape(-1,1), dw1))
        dw2 = np.hstack((dw1.index.to_numpy().reshape(-1, 1) - min(dw1.index), dw1))
    else:
        dw2 = dw1
    if log_sig:
        sig = iis.logsig(dw2, log_sig_prep)
    else:
        sig = iis.sig(dw2, signature_levels)
    # Hack for now
    if np.sum(np.abs(sig)) < 0.0000001:
        # import pdb; pdb.set_trace()
        return (None, None, None)
    # idxs = (dw1.index[0], dw1.index[-1])
    idxs = dw1.index
    return (sig, idxs, dw2)


def yield_window_signatures_non_uni(ds,
                                    signature_levels=3,
                                    window_len=10000,
                                    filternan=True,
                                    gauss_width=None,
                                    time_augmentation=True,
                                    prescale=True,
                                    log_sig=False,
                                    sliding_factor=4,
                                    random_sample_count=None):
    # TODO Check boundry effects here. Because we are pre-windowing this might lead to extra distinction between mal/ben
    # Note: Rolling convolution is feasible in practice
    prep=None
    if gauss_width:
        ds = ds.fillna(0).transform(lambda x: np.convolve(
            x, gauss_filter(gauss_width, 1), mode="same"))
    if prescale:
        alpha = np.math.factorial(signature_levels)**(1 / signature_levels)
        ds = alpha * ds
    if log_sig:
        ncols = ds.shape[1]
        if time_augmentation:
            ncols += 1
        prep = iis.prepare(ncols, signature_levels)
    print("Trace Length", len(ds))
    print("num_windows", len(ds) / window_len)
    print("window len", window_len)
    if random_sample_count:
        bs = np.random.choice(len(ds) - int(window_len / sliding_factor), random_sample_count)
        for b in bs:
            (sig, idxs, dw2) = get_local_signatures(ds,
                                                    base=b,
                                                    signature_levels=signature_levels,
                                                    window_len=window_len,
                                                    filternan=filternan,
                                                    time_augmentation=time_augmentation,
                                                    log_sig=log_sig,
                                                    log_sig_prep=prep)
            if sig is None or idxs is None or dw2 is None:
                continue
            yield (sig, idxs, dw2)
    else:
        for b in range(0, len(ds), int(window_len / sliding_factor)):
            (sig, idxs, dw2) = get_local_signatures(ds,
                                                    base=b,
                                                    signature_levels=signature_levels,
                                                    window_len=window_len,
                                                    filternan=filternan,
                                                    time_augmentation=time_augmentation,
                                                    log_sig=log_sig,
                                                    log_sig_prep=prep)
            if sig is None or idxs is None or dw2 is None:
                continue
            yield (sig, idxs, dw2)
    # return (sigs, idx)


def yield_target_window_signatures_non_uni(ds,
                                           lws,
                                           signature_levels=3,
                                           window_len=10000,
                                           filternan=True,
                                           gauss_width=None,
                                           time_augmentation=True,
                                           prescale=True,
                                           log_sig=False,
                                           sliding_factor=4):
    """
    This function will scan each target window in malware only training. 
    This function is much faster and yielding labeled windows than yield_window_signatures_non_uni -> post checking.
    Basically the same as yield_window_signatures_non_uni, except this function will 
    iterate over each window +- some random offset and then yield the windows in that range.
    """
    # TODO Check boundry effects here. Because we are pre-windowing this might lead to extra distinction between mal/ben
    # Note: Rolling convolution is feasible in practice
    if gauss_width:
        ds = ds.fillna(0).transform(lambda x: np.convolve(
            x, gauss_filter(gauss_width, 1), mode="same"))
    if prescale:
        alpha = np.math.factorial(signature_levels)**(1 / signature_levels)
        ds = alpha * ds
    if log_sig:
        ncols = ds.shape[1]
        if time_augmentation:
            ncols += 1
        prep = iis.prepare(ncols, signature_levels)
    print("Trace Length", len(ds))
    print("num_windows", len(ds) / window_len)
    print("window len", window_len)
    for w in lws:
        prep = None
        window_step  = int(window_len / sliding_factor)
        # Add some variability in the training windows
        start_offset = random.randint(0, int(window_step / 2))
        stop_offset  = random.randint(0, int(window_step / 2))
        start = ds.index.get_loc(min(w.range), method='backfill')
        stop = ds.index.get_loc(max(w.range), method='backfill')
        for b in range(start - start_offset, stop + stop_offset, window_step):
            (sig, idxs, dw2) = get_local_signatures(ds,
                                                    base=b,
                                                    signature_levels=signature_levels,
                                                    window_len=window_len,
                                                    filternan=filternan,
                                                    time_augmentation=time_augmentation,
                                                    log_sig=log_sig,
                                                    log_sig_prep=prep)
            if sig is None or idxs is None or dw2 is None:
                continue
            yield (sig, idxs, dw2)
        # return (sigs, idx)


# For now just store the percentage of each label in a window
class PathSigExtractor:
    def __init__(self,
                 root,
                 signature_levels=3,
                 window_len=400,
                 filternan=True,
                 window_label_thresh=0.2,
                 BENIGN_LBL=0,
                 gauss_width=None,
                 time_augmentation=True,
                 subsample=None,
                 drop_op=True,
                 filter_op=True,
                 prescale=True,
                 random_sample_count=None):
        assert window_label_thresh >= 0.0 and window_label_thresh <= 1.0, \
            "Invalid window label threshold, should be in [0,1]"
        self.root = root
        self.signature_levels = signature_levels
        self.window_len = window_len
        self.filternan = filternan
        self.window_label_thresh = window_label_thresh
        self.BENIGN_LBL = BENIGN_LBL
        self.gauss_width = gauss_width
        self.time_augmentation = time_augmentation
        self.subsample = subsample
        self.drop_op = drop_op
        self.filter_op = filter_op
        self.prescale = prescale,
        self.random_sample_count = random_sample_count
        # TODO, fix indexes for this
        if filter_op:
            self.drop_op = True

    def get_base_trace(self, fname):
        boot_crap_offset = 10000
        drop_op = self.drop_op
        # TODO Migrate this to TraceFile as it actually has logging capabilities
        d, lw, wt = load_trace(self.root, fname)
        if "-na.out" in fname:
            lw = [LabeledWindow(d.index[0], d.index[-1], self.BENIGN_LBL)]
            wt = WindowTree(lw)
        d = d.iloc[boot_crap_offset:]
        # This column is useful for analysis but can screw with path signatures
        if self.filter_op:
            d = d[d.op.isnull()]
        if drop_op:
            d = d.drop("op", axis=1)
            print(d.head())
        if self.subsample:
            w = self.subsample
            # Rolling window must come before reindex??
            # d = d.reindex(range(d.index[0], d.index[-1])).fillna(0).rolling(w).sum()
            # If I do a rolling window do I really need to reindex? Default just sum and interpolate should be similar to sampling in the PMU
            d = d.fillna(0).rolling(w).sum().iloc[w:]
            # d = d.reindex(range(d.index[0], d.index[-1])).interpolate(method="linear")
        return d, lw, wt

    def __call__(self, fname):
        d, lw, wt = self.get_base_trace(fname)
        sl = self.signature_levels
        sigs, ranges = get_window_signatures(
            d,
            lw,
            signature_levels=sl,
            window_len=self.window_len,
            filternan=self.filternan,
            gauss_width=self.gauss_width,
            time_augmentation=self.time_augmentation,
            prescale=self.prescale)
        labels = []
        for rng in ranges:
            lbls = defaultdict(int)
            for i in range(*rng):
                lbls[wt[i]] += 1
            for i in lbls:
                lbls[i] /= self.window_len
            labels.append(lbls)
        return (sigs, labels)

    def get_signatures(self, fname):
        d, lw, wt = self.get_base_trace(fname)
        sl = self.signature_levels
        for sig, idxs, dw in yield_window_signatures_non_uni(
                d,
                signature_levels=sl,
                window_len=self.window_len,
                filternan=self.filternan,
                gauss_width=self.gauss_width,
                time_augmentation=self.time_augmentation,
                random_sample_count=self.random_sample_count):
            lbls = defaultdict(int)
            for i in idxs:
                lbl, soft_labels = wt.get_all_labels_for_key(i)
                lbls[lbl] += 1
                for sl in soft_labels:
                    lbls[sl] += 1
            for i in lbls:
                lbls[i] /= self.window_len
            yield (sig, lbls, dw)
    
    def get_mal_window_signatures(self, fname):
        """
        Basically, the same thing as get_signatures except 
        we focus on detection windows surrounding the target labeled windows.
        for each labeled_window in labeled_windows:
            start_loc = min(labeled_window.range)
            start_loc = labeled_window +- random_offset
            yield_windows_in_range(df, start_loc, max(labeled_window.range))
        """
        d, lw, wt = self.get_base_trace(fname)
        sl = self.signature_levels
        for sig, idxs, dw in yield_target_window_signatures_non_uni(
                d, lw,
                signature_levels=sl,
                window_len=self.window_len,
                filternan=self.filternan,
                gauss_width=self.gauss_width,
                time_augmentation=self.time_augmentation):
            lbls = defaultdict(int)
            for i in idxs:
                lbl, soft_labels = wt.get_all_labels_for_key(i)
                lbls[lbl] += 1
                for sl in soft_labels:
                    lbls[sl] += 1
            for i in lbls:
                lbls[i] /= self.window_len
            yield (sig, lbls, dw)

if __name__ == '__main__':
    import os
    from tqdm import tqdm
    fn = "event-traces/batch8/spectre/spectre.out-D0-B0-1612287273.log"
    root = os.path.dirname(fn)
    tgt = os.path.basename(fn)
    PSE = PathSigExtractor(root)
    for sig, lbl, dw in tqdm(PSE.get_signatures(tgt)):
        pass

