import logging
import coloredlogs
import time
import json
from numpy import float32 as np_float32
from akiraDB.db.AkiraDB import AkiraDB
from .FileLoaders import TraceFile

logger = logging.getLogger(__name__)
coloredlogs.install(level=logger.getEffectiveLevel())


def processFile(engine, fname, force_benign=False, force=False, override_has_malicious=False, additional_meta=[], script_run="", to_float32=True, db_write_kwargs={}):
    assert not (force_benign and override_has_malicious), "Cannot force benign and malicious at the same time"
    logger.info("Processing File %s, force_benign=%s", fname, force_benign)
    akiraDB = AkiraDB(engine)
    # traceDB = getTraceStore(engine)
    # lwDB = getLabeledWindowStore(engine)

    has_malicious_bits = False
    # Hack
    if "-na.out" in fname:
        force_benign = True
    trace = TraceFile(fname, force_benign=force_benign)
    if len(trace.labels) > 1:
        logger.info("Found Malicious bits in Trace")
        has_malicious_bits = True
    if override_has_malicious:
        logger.info("(Override) Setting has_malicious_flag")
        has_malicious_bits = True
    # trace_name = trace.id
    # Parse m5Log -> (Pid -> program map) later
    # Only need to store the log for now
    m5Log = trace.read_m5_log()
    metadata = {"labels": list(trace.labels),
                "trace_file": trace.trace_file,
                "trace_len": len(trace.data_frame),
                "default_benign_label": trace.default_benign_label,
                "has_malicious_bits": has_malicious_bits,
                "comments": "",
                "programs_run": [],
                "script_run": script_run,
                "m5log": m5Log,
                "additional_metadata": additional_meta
                }
    if "probe_config" in db_write_kwargs:
        metadata["probe_config"] = json.dumps(db_write_kwargs["probe_config"])

    # Make sure we havent already processed this trace
    if akiraDB.has_symbol(trace.id) and not force:
        logger.warning("Trace %s Already exists. aborting", trace.id)
        return
    logger.info("Commiting %s to DB", trace.id)
    t0 = time.time()
    metadata['trace_shape'] = trace.data_frame.shape
    if to_float32:
        # Hack
        trace._d = trace.data_frame.astype(np_float32)
    akiraDB.write(trace, metadata=metadata, **db_write_kwargs)
    logger.info("Commiting %s to DB (Done in %d s)", trace.id, time.time() - t0)
