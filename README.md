# AkiraDB The goto Akira database tooling

This is the goto package for all dataset interactions. It includes:
- Basic orchestration for the MinIO object storage
- A Python package for dataset types and interaction

## Standalone instance

1. Modify volumes in `docker-compose.yml` to point to your device volume
2. `docker-compose build`
3. `docker-compose run` 
4. Point browser to `http://localhost:9001`
